# Conway's Game of Life
A simple TUI for for Conway's Game of Life.

# Usage
After installing the Rust toolchain, build it with `cargo build --release`, or run it now with `cargo run --release`. \
Move the cursor with your arrow keys, and press `Enter` to step time by 1. \
`Tab` can be pressed to step time by 10, with a 100ms delay per step. \
Quit with `Esc`.