use console::Term;

use std::num::NonZeroUsize;
use std::fmt::Display;

use crate::cell::{Cell, CellState};

pub struct Grid<'a> {
    grid: Vec<Vec<Cell>>,
    term: &'a Term
}

impl<'a> Grid<'a> {
    pub fn new(rows: NonZeroUsize, columns: NonZeroUsize, term: &'a Term) -> Grid<'a> {
        let mut grid = Vec::with_capacity(rows.get());
        for _ in 0..rows.into() {
            let mut row = Vec::with_capacity(columns.get());
            for _ in 0..columns.into() {
                row.push(Cell::default());
            }
            grid.push(row)
        }
        Grid {
            grid,
            term,
        }
    }
    
    pub fn get_cell(&self, row: usize, column: usize) -> Option<&Cell> {
        self.grid.get(row)?.get(column)
    }
    pub fn get_cell_mut(&mut self, row: usize, column: usize) -> Option<&mut Cell> {
        self.grid.get_mut(row)?.get_mut(column)
    }
    pub fn calculate_state(&self, row: usize, column: usize) -> Option<CellState> {
        let cell = self.get_cell(row, column)?;
        let mut alive_cells = 0;
        
        // I'm aware this is not the most efficient or "idiomatic" method, but it performs well enough for this simple project.
        
        //   0 1 2
        // 0 D D D
        // 1 D A D
        // 2 D D D
        // HORIZONTAL: LEFT (0, -1), RIGHT (0, 1) 
        // VERTICAL: ABOVE (-1, 0), BELOW (1, 0)
        // TOP_DIAGONAL: TOP_LEFT (-1, -1), TOP_RIGHT (-1, 1)
        // BOTTOM_DIAGONAL: BOTTOM_LEFT (1, -1), BOTTOM_RIGHT (1, 1)
        // 1: (0, -1), 2: (0, 1), 3: (-1, 0), 4: (1, 0), 5: (-1, -1), 6: (-1, 1), 7: (1, -1), 8: (1, 1)

        // HORIZONTAL_LEFT: (0, -1)
        if let Some(column) = column.checked_sub(1) {
            if let Some(cell) = self.get_cell(row, column) {
                if cell.state().is_alive() {
                    alive_cells += 1;
                }
            } 
        }

        // HORIZONTAL_RIGHT: (0, 1)
        if let Some(column) = column.checked_add(1) {
            if let Some(cell) = self.get_cell(row, column) {
                if cell.state().is_alive() {
                    alive_cells += 1;
                }
            } 
        }

        // VETICAL_ABOVE: (-1, 0)
        if let Some(row) = row.checked_sub(1) {
            if let Some(cell) = self.get_cell(row, column) {
                if cell.state().is_alive() {
                    alive_cells += 1;
                }
            } 
        }

        // VETICAL_BELOW: (1, 0)
        if let Some(row) = row.checked_add(1) {
            if let Some(cell) = self.get_cell(row, column) {
                if cell.state().is_alive() {
                    alive_cells += 1;
                }
            } 
        } 

        // TOP_LEFT_DIAG: (-1, -1)
        if let (Some(row), Some(column)) = (row.checked_sub(1), column.checked_sub(1)) {
            if let Some(cell) = self.get_cell(row, column) {
                if cell.state().is_alive() {
                    alive_cells += 1;
                }
            } 
        } 

        // TOP_RIGHT_DIAG: (-1, 1)
        if let (Some(row), Some(column)) = (row.checked_sub(1), column.checked_add(1)) {
            if let Some(cell) = self.get_cell(row, column) {
                if cell.state().is_alive() {
                    alive_cells += 1;
                }
            } 
        } 

        // BOTTOM_LEFT_DIAG: (1, -1)
        if let (Some(row), Some(column)) = (row.checked_add(1), column.checked_sub(1)) {
            if let Some(cell) = self.get_cell(row, column) {
                if cell.state().is_alive() {
                    alive_cells += 1;
                }
            } 
        }

        // BOTTOM_LEFT_DIAG: (1, 1)
        if let (Some(row), Some(column)) = (row.checked_add(1), column.checked_add(1)) {
            if let Some(cell) = self.get_cell(row, column) {
                if cell.state().is_alive() {
                    alive_cells += 1;
                }
            } 
        }

        if cell.state().is_alive() {
            if alive_cells < 2 {
                Some(CellState::Dead)
            } else if alive_cells > 3 {
                Some(CellState::Dead)
            } else {
                Some(CellState::Alive)
            }
        } else {
            if alive_cells == 3 {
                Some(CellState::Alive)
            } else {
                Some(CellState::Dead)
            }
        }
    }
    pub fn display(&self) {
        self.term.clear_screen().unwrap();
        print!("{self}");
    }
    pub fn tick(&mut self) {
        let mut new_grid: Vec<Vec<Cell>> = Vec::with_capacity(self.grid.len());
        for i  in 0..(self.grid.len()) {
            new_grid.push(Vec::with_capacity(self.grid[0].len()));
            for j in 0..(self.grid[i].len()) {
                let new_state = self.calculate_state(i, j).unwrap();
                new_grid[i].push(Cell::from_state(new_state));
            }
        }
        self.grid = new_grid;
    }
}

impl Display for Grid<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in &self.grid {
            for cell in row {
                if cell.state().is_alive() {
                    write!(f, "■")?;
                } else {
                    write!(f, "□")?;
                }
            }
                writeln!(f)?;
        }
        Ok(())
    }
}
