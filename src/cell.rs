#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum CellState {
    Alive,
    #[default]
    Dead,
}
impl CellState {
    pub fn is_alive(&self) -> bool {
        if let CellState::Alive = self {
            true
        } else {
            false
        }
    }
}

#[derive(Default, Debug, PartialEq, Eq)]
pub struct Cell {
    state: CellState,
}

impl Cell {
    pub fn from_state(state: CellState) -> Self {
        Cell {
            state,
        }
    }
    pub fn toggle_state(&mut self) {
        match self.state {
            CellState::Alive => self.state = CellState::Dead,
            CellState::Dead => self.state = CellState::Alive,
        }
    }
    pub fn set_state(&mut self, state: CellState) {
        self.state = state
    }
    pub fn state(&self) -> CellState {
        self.state
    }
}