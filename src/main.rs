use std::num::NonZeroUsize;
use console::{Key, Term};
use std::time::Duration;
use std::thread;
use std::error::Error;

use crate::grid::Grid;

pub mod cell;
pub mod grid;

pub fn move_cursor(pos: &mut (usize, usize), term: &Term, change_x: isize, change_y: isize) -> Result<(), Box<dyn Error>> {
    term.move_cursor_to(((pos.0 as isize) + change_x).try_into().unwrap_or(pos.0), ((pos.1 as isize) + change_y).try_into().unwrap_or(pos.1))?;
    pos.0 = (pos.0 as isize + change_x).try_into().unwrap_or(pos.0);
    pos.1 = (pos.1 as isize + change_y).try_into().unwrap_or(pos.0);
    Ok(())
}
fn main() -> Result<(), Box<dyn Error>> {
    let term = Term::stdout();

    // Why subtract the term rows by 1? I don't know, but it works great.
    let mut grid = Grid::new(NonZeroUsize::new((term.size().0 - 1) as usize).unwrap_or(NonZeroUsize::new(16).unwrap()), NonZeroUsize::new((term.size().1) as usize).unwrap_or(NonZeroUsize::new(16).unwrap()), &term);
    grid.display();
    let mut cursor_pos = (0, 0);

    loop {
        term.move_cursor_to(cursor_pos.0, cursor_pos.1)?;
        match term.read_key()? {
            Key::ArrowLeft => move_cursor(&mut cursor_pos, &term, -1, 0)?,
            Key::ArrowRight => move_cursor(&mut cursor_pos, &term, 1, 0)?,
            Key::ArrowUp => move_cursor(&mut cursor_pos, &term, 0, -1)?,
            Key::ArrowDown => move_cursor(&mut cursor_pos, &term, 0, 1)?,
            Key::Char(' ') => match grid.get_cell_mut(cursor_pos.1, cursor_pos.0) {
                Some(cell) => {
                    cell.toggle_state();
                    grid.display();
                },
                None => {},
            }
            Key::Enter => {
                grid.tick();
                grid.display();
            }
            Key::Tab => for _ in 0..10 {
                grid.tick();
                grid.display();
                thread::sleep(Duration::from_millis(100))
            }
            Key::Escape => break Ok(()),
            _ => continue,
        }
    }  
}